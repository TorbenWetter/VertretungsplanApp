import React from 'react';
import { Alert, AppState, AsyncStorage, BackHandler, Dimensions, Text, TouchableOpacity, View } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Icon from 'react-native-vector-icons/FontAwesome';
import RNC from 'react-native-css';
import { Notifications } from 'expo';
import * as Font from 'expo-font';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from './Toast.js';
import { style } from './Styles.js';

export default class PlanScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Plan auswählen',
    headerBackTitle: 'Zurück zu den Plänen',
    headerTruncatedBackTitle: 'Pläne',
    headerLeft: (
      <Icon.Button
        name='graduation-cap'
        backgroundColor='transparent'
        color='#FFFFFF'
        style={style.IconButton}
        iconStyle={style.IconButtonIcon}
        onPress={() => {
          const { navigate } = navigation;
          Alert.alert(
            'Auswahl ändern',
            'Möchtest Du eine andere Klasse oder andere Kurse auswählen?\n\nAchtung: Deine aktuelle Auswahl wird nach Klicken auf "Ja" gelöscht.',
            [
              {
                text: 'Nein',
                style: 'cancel'
              },
              {
                text: 'Ja',
                onPress: () => navigate('Grade', { deleteFile: true })
              }
            ],
            { cancelable: false }
          );
        }}
      />
    ),
    headerRight: (
      <Icon.Button
        name='sliders'
        backgroundColor='transparent'
        color='#FFFFFF'
        style={style.IconButton}
        iconStyle={style.IconButtonIcon}
        onPress={() => {
          const { navigate } = navigation;
          navigate('Settings');
        }}
      />
    ),
    gesturesEnabled: false
  });

  state = {
    appState: AppState.currentState,
    connected: true,
    isLoading: true,
    fontLoaded: false,
    orientationLandscape: Dimensions.get('window').width > Dimensions.get('window').height,
    plans: ''
  };

  handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      const notification = global.notification,
        goToDataScreen = notification !== undefined;
      if (goToDataScreen) {
        global.notification = undefined;
        this.props.navigation.navigate('Plan', { goToDataScreen: goToDataScreen, data: goToDataScreen ? notification.data : null });
      }
    }
    this.setState({ appState: nextAppState });
  }

  handleNotifications = (notification) => {
    global.notification = notification;
  };

  onBackButtonClicked = () => {
    BackHandler.exitApp();
    return true;
  }

  getPlans = () => {
    try {
      AsyncStorage.getItem('password')
        .then(async (password) => {
          if (password !== null)
            AsyncStorage.getItem('classes')
              .then(async (classes) => {
                if (classes !== null) {
                  fetch('https://wetter.codes/gg/plans/' + classes, {
                    method: 'POST',
                    body: JSON.stringify({
                      'password': password
                    }),
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    }
                  })
                    .then(async (response) => response.json())
                    .then(async (responseJson) => {
                      if (responseJson.valid) {
                        this.setState({
                          isLoading: false,
                          plans: JSON.stringify(responseJson.plans)
                        });

                        Notifications.setBadgeNumberAsync(0);

                        const { goToDataScreen, data } = this.props.route.params;
                        if (goToDataScreen) {
                          const { navigate } = this.props.navigation;
                          navigate('Data', { date: data.date, infos: data.infos, lessons: data.lessons });
                        }
                      } else {
                        try {
                          AsyncStorage.removeItem('password');
                          const { navigate } = this.props.navigation;
                          navigate('Start');
                          Alert.alert(
                            'Fehler',
                            'Das gespeicherte Passwort ist nicht mehr korrekt.\nBitte gib das neue Passwort ein.',
                            [{
                              text: 'OK'
                            }],
                            { cancelable: false }
                          );
                        } catch (error) {
                          console.error(error);
                        }
                      }
                    })
                    .catch(error => console.error(error))
                    .done();
                }
              }).done();
        }).done();
    } catch (error) {
      console.error(error);
    }
  }

  handleConnectionChange = (isConnected) => {
    this.setState({ connected: isConnected });
    if (isConnected && this.state.plans === '')
      this.getPlans();
  };

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonClicked);

    AppState.addEventListener('change', this.handleAppStateChange);

    Notifications.addListener(this.handleNotifications);

    await Font.loadAsync({
      'sourcesanspro-semibold': require('./assets/fonts/SourceSansPro-SemiBold.ttf')
    });

    this.setState({ fontLoaded: true });

    this.unsubscribeNetInfo = NetInfo.addEventListener(state => this.handleConnectionChange(state.isConnected));

    NetInfo.fetch().then(state => {
      this.setState({ connected: state.isConnected });
      if (state.isConnected)
        this.getPlans();
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonClicked);

    AppState.removeEventListener('change', this.handleAppStateChange);

    Notifications.removeListener(this.handleNotifications);

    this.unsubscribeNetInfo();
  }

  onLayout = (e) => {
    const { width, height } = Dimensions.get('window');
    this.setState({ orientationLandscape: width > height })
  }

  render() {
    if (!this.state.isLoading && this.state.fontLoaded) {
      const planButtons = [],
        plans = JSON.parse(this.state.plans);

      plans.map((plan, i) => {
        planButtons.push(
          <TouchableOpacity
            key={plan.date}
            onPress={() => {
              plan = JSON.parse(this.state.plans)[i]
              const { navigate } = this.props.navigation;
              navigate('Data', { date: plan.date, infos: plan.data.infos, lessons: plan.data.lessons });
            }}
          >
            <View style={[cssStyles.dateBadge, style.Badge, style.PlanBadge]}>
              <Text style={[style.BadgeText, {
                fontFamily: 'sourcesanspro-semibold'
              }]}>{plan.date.split(', ')[1]}</Text>
            </View>

            <View style={[cssStyles.container, style.Container, style.PlanContainer]}>
              <Text style={[style.ContainerText, style.PlanContainerText, {
                fontFamily: 'sourcesanspro-semibold'
              }]}>{plan.date.split(', ')[0]}</Text>
            </View>
          </TouchableOpacity>
        );

        if (i != plans.length - 1)
          planButtons.push(
            <View key={'placeholder' + i} style={style.PlanPlaceholder} />
          );
      });

      return (
        <View style={[style.MainView, style.PlanMainView]}>
          <View onLayout={this.onLayout.bind(this)} style={{
            flexDirection: this.state.orientationLandscape ? 'row' : 'column'
          }}>
            {
              plans.length == 0 ?
                <Text style={[style.InfoText, style.PlanInfoText, {
                  fontFamily: 'sourcesanspro-semibold'
                }]}>Aktuell sind keine Vertretungspläne verfügbar.</Text> : null
            }
            {planButtons}
          </View>
          <Toast
            backgroundColor='#ef8181'
            containerStyle={style.Toast}
            hideOnPress={false}
            position={100}
            shadowColor='white'
            textColor='black'
            textStyle={style.ToastText}
            visible={!this.state.connected}
          >Keine Internetverbindung!</Toast>
        </View>
      );
    } else if (!this.state.connected)
      return (
        <View style={[style.MainView, style.PlanMainView]}>
          <Toast
            backgroundColor='#ef8181'
            containerStyle={style.Toast}
            hideOnPress={false}
            position={100}
            shadowColor='white'
            textColor='black'
            textStyle={style.ToastText}
            visible={!this.state.connected}
          >Keine Internetverbindung!</Toast>
        </View>
      );
    else
      return (
        <View style={[style.MainView, style.PlanMainView]}>
          <Spinner
            cancelable={false}
            color='#FFBC42'
            animation='slide'
            overlayColor='transparent'
            size='large'
            textContent='Suche nach Plänen..'
            textStyle={style.SpinnerText}
            visible={this.state.isLoading || !this.state.fontLoaded}
          />
        </View>
      );
  }
}

const cssStyles = RNC`
  container {
    z-index: 1;
  }

  dateBadge {
    z-index: 2;
  }
`;
