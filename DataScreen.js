import React from 'react';
import { BackHandler, ScrollView, Text, View } from 'react-native';
import RNC from 'react-native-css';
import * as Font from 'expo-font';
import { style } from './Styles.js';

export default class DataScreen extends React.Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    title: navigation.state.params.date
  });

  state = {
    fontLoaded: false
  };

  onBackButtonClicked = () => {
    const { goBack } = this.props.navigation;
    goBack();
    return true;
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonClicked);

    await Font.loadAsync({
      'sourcesanspro-semibold': require('./assets/fonts/SourceSansPro-SemiBold.ttf')
    });

    this.setState({ fontLoaded: true });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonClicked);
  }

  render() {
    if (this.state.fontLoaded) {
      const { infos, lessons } = this.props.route.params;
      if (infos.length == 0 && lessons.length == 0) {
        return (
          <View style={style.MainView}>
            <View style={style.CenterMainView}>
              <Text style={[style.InfoText, style.DataInfoText, {
                fontFamily: 'sourcesanspro-semibold'
              }]}>
                Für diesen Tag sind weder allgemeine Informationen verfügbar, noch Vertretungsstunden für Dich relevant.
              </Text>
            </View>
            <Text style={[style.InfoText, style.DataBottomInfoText, {
              fontFamily: 'sourcesanspro-semibold'
            }]}>Verbindlich ist nur der in der Schule angezeigte Vertretungsplan!</Text>
          </View>
        );
      } else {
        const dataTexts = [];

        infos.map((info, i) => {
          dataTexts.push(
            <View key={"info" + i}>
              <View style={[cssStyles.badges, style.Badges]}>

                <View style={[style.Badge, style.DataBadge, style.InfoBadge]}>
                  <Text style={[style.BadgeText, {
                    fontFamily: 'sourcesanspro-semibold'
                  }]}>Information</Text>
                </View>

              </View>

              <View style={[cssStyles.container, style.Container, style.DataContainer]}>
                <Text style={[style.ContainerText, style.DataContainerText, {
                  fontFamily: 'sourcesanspro-semibold'
                }]}>{info}</Text>
              </View>

            </View>
          );
        });

        lessons.map((lesson, i) => {
          const lessonName = lesson.shift(),
            category = lesson.shift(),
            typeColor = lesson.pop(),
            otherInfos = lesson.filter(Boolean);
          dataTexts.push(
            <View key={"lesson" + i}>
              <View style={[cssStyles.badges, style.Badges]}>

                <View style={[style.Badge, style.DataBadge, style.LessonBadge]}>
                  <Text style={[style.BadgeText, {
                    fontFamily: 'sourcesanspro-semibold'
                  }]}>{lessonName}</Text>
                </View>

                <View style={[style.Badge, style.DataBadge, {
                  backgroundColor: typeColor ? typeColor : '#FFFFFF'
                }]}>
                  <Text style={[style.BadgeText, {
                    fontFamily: 'sourcesanspro-semibold'
                  }]}>{category}</Text>
                </View>

              </View>

              <View style={[cssStyles.container, style.Container, style.DataContainer]}>
                <Text style={[style.ContainerText, style.DataContainerText, {
                  fontFamily: 'sourcesanspro-semibold'
                }]}>{otherInfos.join('\n')}</Text>
              </View>

            </View>
          );
        });

        return (
          <View style={style.MainView}>
            <ScrollView>
              {dataTexts}
            </ScrollView>
            <Text style={[style.InfoText, style.DataBottomInfoText, {
              fontFamily: 'sourcesanspro-semibold'
            }]}>Verbindlich ist nur der in der Schule angezeigte Vertretungsplan!</Text>
          </View>
        );
      }
    } else
      return (<View style={style.MainView} />);
  }
}

const cssStyles = RNC`
  container {
    z-index: 1;
  }

  badges {
    z-index: 2;
  }
`;
