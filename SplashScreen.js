import React from 'react';
import { AsyncStorage, View } from 'react-native';
import { Notifications } from 'expo';
import Spinner from 'react-native-loading-spinner-overlay';
import { style } from './Styles.js';

export default class SplashScreen extends React.Component {
  static navigationOptions = {
    title: 'Bitte warte',
    headerLeft: null
  };

  state = {
    loading: true,
    notification: undefined
  };

  handleNotifications = (notification) => {
    this.setState({ notification: notification });
  };

  async componentDidMount() {
    Notifications.addListener(this.handleNotifications);

    try {
      AsyncStorage.getItem('password')
        .then(async (password) => {
          const { navigate } = this.props.navigation;
          if (password !== null) {
            try {
              AsyncStorage.getItem('classes')
                .then(async (classes) => {
                  if (classes === null) {
                    this.setState({ loading: false });
                    navigate('Grade', { deleteFile: false });
                  } else {
                    const { notification } = this.state,
                      goToDataScreen = notification !== undefined;
                    this.setState({
                      loading: false,
                      notification: undefined
                    });
                    navigate('Plan', { goToDataScreen: goToDataScreen, data: goToDataScreen ? notification.data : null });
                  }
                }).done();
            } catch (error) {
              console.error(error);
            }
          } else {
            this.setState({ loading: false });
            navigate('Start');
          }
        }).done();
    } catch (error) {
      console.error(error);
    }
  }

  componentWillUnmount() {
    Notifications.removeListener(this.handleNotifications);
  }

  render() {
    return (
      <View style={[style.MainView]}>
        <Spinner
          cancelable={false}
          color='#FFBC42'
          animation='slide'
          overlayColor='transparent'
          size='large'
          textContent='Überprüfe Passwort..'
          textStyle={style.SpinnerText}
          visible={this.state.loading}
        />
      </View>
    );
  }
}
