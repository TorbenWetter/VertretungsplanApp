import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { View } from 'react-native-animatable'
import PropTypes from 'prop-types'
import RNC from 'react-native-css';
import { moderateScale, verticalScale } from 'react-native-size-matters';
import * as Font from 'expo-font';

export default class RadioButton extends Component {
  static propTypes = {
    isSelected: PropTypes.bool,
    onPress: PropTypes.func,
    label: PropTypes.string,
    show: PropTypes.bool,
    marginHoriz: PropTypes.string,
    textSize: PropTypes.number
  }

  static defaultProps = {
    isSelected: false,
    onPress: () => null,
    label: '',
    show: true,
    marginHoriz: '0%',
    textSize: 16
  }

  state = {
    fontLoaded: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      'sourcesanspro-semibold': require('./assets/fonts/SourceSansPro-SemiBold.ttf')
    });

    this.setState({
      fontLoaded: true
    });
  }

  render() {
    const { isSelected, onPress, label, show, marginHoriz, textSize } = this.props,
      size = 20 * StyleSheet.hairlineWidth,
      outerStyle = {
        borderColor: '#FFBC42',
        width: 2 * size,
        height: 2 * size,
        borderRadius: size,
        borderWidth: size / 5,
        backgroundColor: '#2E3649'
      },
      innerStyle = {
        width: size,
        height: size,
        borderRadius: size / 2,
        backgroundColor: '#FFBC42'
      };

    if (show && this.state.fontLoaded)
      return (
        <TouchableOpacity
          onPress={onPress}
        >
          <View style={[cssStyles.badges, styles.Badges, {
            top: size,
            left: size / 5,
            marginLeft: (3 + Number(new RegExp('^[0-9]+').exec(marginHoriz).toString())).toString() + '%'
          }]}>

            <View style={[styles.Radio, outerStyle]}>
              {isSelected ? <View style={innerStyle} {...this.props} /> : null}
            </View>

          </View>

          <View style={[cssStyles.container, styles.Container, {
            marginHorizontal: marginHoriz
          }]}>
            <Text style={[styles.ContainerText, {
              fontSize: moderateScale(textSize)
            }]}>{label}</Text>
          </View>

        </TouchableOpacity>
      )
    else
      return (<View />)
  }
}

const cssStyles = RNC`
  container {
    z-index: 1;
  }

  badges {
    z-index: 2;
  }
`;

const styles = StyleSheet.create({
  MainView: {
    alignItems: 'center',
    backgroundColor: '#1C2431',
    flex: 1,
    justifyContent: 'center'
  },
  Container: {
    backgroundColor: '#2E3649',
    borderRadius: moderateScale(15),
    padding: moderateScale(11),
    paddingTop: moderateScale(14),
    marginBottom: verticalScale(3),
    shadowColor: '#000000',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowOpacity: 0.4,
    shadowRadius: 1
  },
  ContainerText: {
    alignSelf: 'stretch',
    color: 'white',
    fontFamily: 'sourcesanspro-semibold'
  },
  Badges: {
    flexDirection: 'row',
    marginTop: verticalScale(3)
  },
  Radio: {
    justifyContent: 'center',
    alignItems: 'center'
  }
});
