import 'react-native-gesture-handler';

import React from 'react';
import { Platform, StatusBar, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from './SplashScreen';
import StartScreen from './StartScreen';
import GradeScreen from './GradeScreen';
import PlanScreen from './PlanScreen';
import DataScreen from './DataScreen';
import SettingsScreen from './SettingsScreen';

const navigationOptions = {
  orientation: 'portrait',
  headerTitleStyle: {
    color: 'white'
  },
  headerStyle: {
    backgroundColor: '#242728',
    height: Platform.OS === 'ios' ? 44 : 54
  },
  headerTintColor: 'white'
}

const Stack = createStackNavigator();

export default class App extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar barStyle='light-content' backgroundColor='#242728' translucent={true} />
        <NavigationContainer style={{ flex: 1 }}>
          <Stack.Navigator screenOptions={navigationOptions}>
            <Stack.Screen name="Splash" component={SplashScreen} />
            <Stack.Screen name="Start" component={StartScreen} />
            <Stack.Screen name="Grade" component={GradeScreen} />
            <Stack.Screen name="Plan" component={PlanScreen} />
            <Stack.Screen name="Data" component={DataScreen} />
            <Stack.Screen name="Settings" component={SettingsScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </View>
    );
  }
}
