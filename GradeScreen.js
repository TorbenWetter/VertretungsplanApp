import React from 'react';
import { Alert, AsyncStorage, BackHandler, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import RadioButton from './RadioButton.js';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as Font from 'expo-font';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from './Toast.js';
import { style } from './Styles.js';

export default class GradeScreen extends React.Component {
  static navigationOptions = {
    title: 'Klasse/Kurse auswählen',
    headerLeft: null,
    gesturesEnabled: false
  };

  state = {
    connected: true,
    isLoading: true,
    sendingData: false,
    fontLoaded: false,
    radioProps: {},
    radioSelected: [],
    radioSelectedRough: [],
    radioSelectedClasses: [],
    radioSelectedCourses: []
  };

  onBackButtonClicked = () => {
    BackHandler.exitApp();
    return true;
  }

  getClasses = () => {
    try {
      AsyncStorage.getItem('password')
        .then(async (password) => {
          if (password !== null)
            AsyncStorage.getItem('classes')
              .then(async (classes) => {
                if (classes !== null) {
                  const { navigate } = this.props.navigation;
                  navigate('Plan', { goToDataScreen: false });
                } else {
                  fetch('https://wetter.codes/gg/classes', {
                    method: 'POST',
                    body: JSON.stringify({
                      'password': password
                    }),
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    }
                  })
                    .then(async (response) => response.json())
                    .then(async (responseJson) => {
                      if (responseJson.valid) {
                        const radioProps = JSON.parse(responseJson.data),
                          radioProps_s = JSON.stringify(radioProps),
                          radioSelected = JSON.parse(radioProps_s.replace(new RegExp('"[a-zA-Z0-9]+"', 'g'), 'false'));
                        this.setState({
                          isLoading: false,
                          radioProps: radioProps,
                          radioSelected: radioSelected,
                          radioSelectedRough: Array(radioProps.length).fill(false),
                          radioSelectedClasses: Array(radioProps[0].length).fill(false),
                          radioSelectedCourses: Array(radioProps[1].length).fill(false)
                        })
                      } else {
                        try {
                          AsyncStorage.removeItem('password');
                          const { navigate } = this.props.navigation;
                          navigate('Start');
                          Alert.alert(
                            'Fehler',
                            'Das gespeicherte Passwort ist nicht mehr korrekt.\nBitte gib das neue Passwort ein.',
                            [{
                              text: 'OK'
                            }],
                            { cancelable: false }
                          );
                        } catch (error) {
                          console.error(error);
                        }
                      }
                    })
                    .catch(error => console.error(error))
                    .done();
                }
              }).done();
        }).done();
    } catch (error) {
      console.error(error);
    }
  }

  handleConnectionChange = (isConnected) => {
    this.setState({ connected: isConnected });
    if (isConnected && this.state.radioSelected.length === 0)
      this.getClasses();
  };

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonClicked);

    await Font.loadAsync({
      'sourcesanspro-semibold': require('./assets/fonts/SourceSansPro-SemiBold.ttf')
    });

    this.setState({ fontLoaded: true });

    this.unsubscribeNetInfo = NetInfo.addEventListener(state => this.handleConnectionChange(state.isConnected));

    const { deleteFile } = this.props.route.params;
    if (deleteFile)
      try {
        AsyncStorage.removeItem('classes');
      } catch (error) {
        console.error(error);
      }

    NetInfo.fetch().then(state => {
      this.setState({ connected: state.isConnected });
      if (state.isConnected)
        this.getClasses();
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonClicked);

    this.unsubscribeNetInfo();
  }

  saveClasses = async (selectedClasses) => {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    var finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      await AsyncStorage.setItem('classes', selectedClasses)
        .then(() => {
          const { navigate } = this.props.navigation;
          navigate('Plan', { goToDataScreen: false });
        }).done();
      return;
    }

    this.setState({ sendingData: true });
    var expoPushToken = await Notifications.getExpoPushTokenAsync();
    try {
      AsyncStorage.getItem('password')
        .then(async (password) => {
          if (password !== null)
            fetch('https://wetter.codes/gg/grade', {
              method: 'POST',
              body: JSON.stringify({
                'password': password,
                'token': expoPushToken,
                'grade': selectedClasses
              }),
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              }
            })
              .then(async (response) => response.json())
              .then(async (responseJson) => {
                this.setState({ sendingData: false });
                if (responseJson.valid) {
                  await AsyncStorage.setItem('classes', selectedClasses)
                    .then(() => {
                      const { navigate } = this.props.navigation;
                      navigate('Plan', { goToDataScreen: false });
                    }).done();
                } else
                  Alert.alert(
                    'Fehler',
                    'Interner Serverfehler. Bitte versuche es zu einem späteren Zeitpunkt erneut.',
                    [{ text: 'OK' }],
                    { cancelable: true }
                  );
              })
              .catch(error => console.error(error))
              .done();
        }).done();
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    if (!this.state.isLoading && this.state.fontLoaded) {
      const onlyTrueInArray = (array, index, parentValue, parentValue2) => {
        var onlyTrue = true;
        array.map((value, i) => {
          if (!parentValue2 || !parentValue || i !== index && value)
            onlyTrue = false;
        });
        return onlyTrue;
      };

      const radioButtons = [],
        radioProps = this.state.radioProps,
        radioRough = ['5.-10. Klasse', '11.-13. Klasse'],
        roughArrays = [this.state.radioSelectedClasses, this.state.radioSelectedCourses];
      var enableButton = false,
        selectedClassName = '';

      radioRough.map((value, i) => {
        radioButtons.push(
          <RadioButton
            key={i}
            animation={'bounceIn'}
            isSelected={this.state.radioSelectedRough[i]}
            onPress={() => {
              this.state.radioSelectedRough[i] = !this.state.radioSelectedRough[i];
              this.forceUpdate();
            }}
            label={value}
            show={!this.state.radioSelectedRough[radioRough.length - 1 - i]}
            marginHoriz='4%'
            textSize={20}
          />
        );

        radioProps[i].map((value2, i2) => {
          radioButtons.push(
            <RadioButton
              key={i + "," + i2}
              animation={'bounceIn'}
              isSelected={this.state.radioSelectedRough[i] && roughArrays[i][i2]}
              onPress={() => {
                roughArrays[i][i2] = !roughArrays[i][i2];
                this.forceUpdate();
              }}
              label={(Number(new RegExp('[a-zA-Z0-9]+').exec(value)) + i2).toString() + '. Klasse'}
              show={onlyTrueInArray(roughArrays[i], i2, this.state.radioSelectedRough[i], this.state.radioSelectedRough[i])}
              marginHoriz='8%'
              textSize={18}
            />
          );

          var selectedCourses = (Number(new RegExp('[a-zA-Z0-9]+').exec(value)) + i2).toString() + '/';
          radioProps[i][i2].map((value3, i3) => {
            if (i == 0) {
              const className = (Number(new RegExp('[a-zA-Z0-9]+').exec(value)) + i2).toString() + this.state.radioProps[i][i2][i3],
                selected = this.state.radioSelectedRough[i] && roughArrays[i][i2] && this.state.radioSelected[i][i2][i3];
              if (selected) {
                selectedClassName = className;
                enableButton = true;
              }
              radioButtons.push(
                <RadioButton
                  key={i + "," + i2 + "," + i3}
                  animation={'bounceIn'}
                  isSelected={selected}
                  onPress={() => {
                    this.state.radioSelected[i][i2][i3] = !this.state.radioSelected[i][i2][i3];
                    this.forceUpdate();
                  }}
                  label={className}
                  show={onlyTrueInArray(this.state.radioSelected[i][i2], i3, roughArrays[i][i2], this.state.radioSelectedRough[i])}
                  marginHoriz='12%'
                  textSize={16}
                />
              );
            } else if (i == 1) {
              radioButtons.push(
                <View
                  style={style.CourseLine}
                  key={i + "," + i2 + "," + i3}
                >
                  {
                    radioProps[i][i2][i3].map((value4, i4) => {
                      const courseName = this.state.radioProps[i][i2][i3][i4],
                        selected = this.state.radioSelectedRough[i] && roughArrays[i][i2] && this.state.radioSelected[i][i2][i3][i4];
                      if (selected) {
                        selectedCourses += courseName + '$';
                        selectedClassName = selectedCourses.substring(0, selectedCourses.length - 1);
                        enableButton = true;
                      }
                      return (
                        <RadioButton
                          key={i + "," + i2 + "," + i3 + "," + i4}
                          animation={'bounceIn'}
                          isSelected={selected}
                          onPress={() => {
                            this.state.radioSelected[i][i2][i3][i4] = !this.state.radioSelected[i][i2][i3][i4];
                            this.forceUpdate();
                          }}
                          label={courseName}
                          show={this.state.radioSelectedRough[i] && roughArrays[i][i2]}
                          textSize={16}
                        />
                      );
                    })
                  }
                </View>
              );
            }
          });
        });
      });

      if (this.state.sendingData)
        enableButton = false;

      return (
        <View style={[style.MainView, style.GradeMainView]}>
          <Text style={[style.InfoText, style.GradeInfoText, {
            fontFamily: 'sourcesanspro-semibold'
          }]}>
            Wähle hier einmalig Deine Klasse bzw. Deine Kurse aus, damit Du immer nur die für Dich relevanten Informationen erhältst.
          </Text>
          <ScrollView>
            {radioButtons}
          </ScrollView>
          {
            enableButton ?
              <TouchableOpacity
                style={[style.SubmitButton, style.GradeSubmitButton, style.SubmitButtonOn]}
                onPress={() => {
                  if (this.state.connected)
                    this.saveClasses(selectedClassName)
                }}
              >
                <Text style={[style.SubmitButtonText, {
                  fontFamily: 'sourcesanspro-semibold'
                }]}>Auswählen</Text>
              </TouchableOpacity> :
              <View
                style={[style.SubmitButton, style.GradeSubmitButton, style.SubmitButtonOff]}
              >
                <Text style={[style.SubmitButtonText, {
                  fontFamily: 'sourcesanspro-semibold'
                }]}>Auswählen</Text>
              </View>
          }
          <Toast
            backgroundColor='#ef8181'
            containerStyle={style.Toast}
            hideOnPress={false}
            position={100}
            shadowColor='white'
            textColor='black'
            textStyle={style.ToastText}
            visible={!this.state.connected}
          >Keine Internetverbindung!</Toast>
        </View>
      );
    } else if (this.state.connected)
      return (
        <View style={[style.MainView, style.GradeMainView]}>
          <Spinner
            cancelable={false}
            color='#FFBC42'
            animation='slide'
            overlayColor='transparent'
            size='large'
            textContent='Lade aktuelle Klassen und Kurse..'
            textStyle={style.SpinnerText}
            visible={this.state.isLoading}
          />
        </View>
      );
    else
      return (
        <View style={[style.MainView, style.GradeMainView]}>
          <Toast
            backgroundColor='#ef8181'
            containerStyle={style.Toast}
            hideOnPress={false}
            position={100}
            shadowColor='white'
            textColor='black'
            textStyle={style.ToastText}
            visible={!this.state.connected}
          >Keine Internetverbindung!</Toast>
        </View>
      );
  }
}
