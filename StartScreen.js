import React from 'react';
import { Alert, AsyncStorage, Text, TextInput, TouchableOpacity, View } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { encrypt } from 'react-native-simple-encryption';
import * as Font from 'expo-font';
import Toast from './Toast.js';
import { style } from './Styles.js';

export default class StartScreen extends React.Component {
  static navigationOptions = {
    title: 'Passwort eingeben',
    headerLeft: null
  };

  state = {
    connected: true,
    fontLoaded: false,
    isLoading: false,
    password: ''
  };

  checkPassword = async () => {
    this.setState({ isLoading: true });
    const password = this.state.password;
    fetch('https://wetter.codes/gg', {
      method: 'POST',
      body: JSON.stringify({
        'password': password,
        'encrypted': false
      }),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(async (response) => response.json())
      .then(async (responseJson) => {
        this.setState({ isLoading: false });
        if (responseJson.valid)
          this.savePassword(responseJson.key, password);
        else {
          Alert.alert(
            'Fehler',
            'Das eingegebene Passwort ist nicht korrekt.',
            [{ text: 'OK' }],
            { cancelable: true }
          );
        }
      })
      .catch(error => console.error(error))
      .done();
  }

  savePassword = async (key, password) => {
    try {
      const encrypted_pw = encrypt(key, password);
      await AsyncStorage.setItem('password', encrypted_pw);
      try {
        AsyncStorage.getItem('classes')
          .then(async (classes) => {
            const { navigate } = this.props.navigation;
            if (classes === null)
              navigate('Grade', { deleteFile: false });
            else
              navigate('Plan', { goToDataScreen: false, data: null });
          }).done();
      } catch (error) {
        console.error(error);
      }
    } catch (error) {
      console.error(error);
    }
  };

  handleConnectionChange = (isConnected) => {
    this.setState({ connected: isConnected });
  };

  async componentDidMount() {
    await Font.loadAsync({
      'sourcesanspro-semibold': require('./assets/fonts/SourceSansPro-SemiBold.ttf')
    });

    this.setState({ fontLoaded: true });

    this.unsubscribeNetInfo = NetInfo.addEventListener(state => this.handleConnectionChange(state.isConnected));

    NetInfo.fetch().then(state => this.setState({ connected: state.isConnected }));
  }

  componentWillUnmount() {
    this.unsubscribeNetInfo();
  }

  render() {
    if (this.state.fontLoaded) {
      return (
        <View style={[style.MainView, style.StartMainView]}>
          <Text style={[style.InfoText, style.StartInfoText, {
            fontFamily: 'sourcesanspro-semibold'
          }]}>
            Gib hier einmalig das Passwort ein, das Du normalerweise auf der Website des Vertretungsplans eingeben musst.
          </Text>
          <TextInput
            autoCapitalize='none'
            autoCorrect={false}
            autoFocus={false}
            keyboardAppearance='dark'
            keyboardType='default'
            multiline={false}
            onChangeText={(text) => this.setState({ password: text })}
            onSubmitEditing={() => {
              if (!this.state.isLoading && this.state.connected && this.state.password.trim().length > 0)
                this.checkPassword();
            }}
            placeholder='Passwort'
            placeholderTextColor='rgba(255, 188, 66, 0.25)'
            returnKeyType='done'
            secureTextEntry={true}
            style={[style.TextField, style.StartTextField, {
              fontFamily: 'sourcesanspro-semibold'
            }]}
            underlineColorAndroid='transparent'
          />
          {
            !this.state.isLoading && this.state.connected && this.state.password.trim().length > 0 ?
              <TouchableOpacity
                style={[style.SubmitButton, style.StartSubmitButton, style.SubmitButtonOn]}
                onPress={() => this.checkPassword()}
              >
                <Text style={[style.SubmitButtonText, {
                  fontFamily: 'sourcesanspro-semibold'
                }]}>Einloggen</Text>
              </TouchableOpacity> :
              <View
                style={[style.SubmitButton, style.StartSubmitButton, style.SubmitButtonOff]}
              >
                <Text style={[style.SubmitButtonText, {
                  fontFamily: 'sourcesanspro-semibold'
                }]}>Einloggen</Text>
              </View>
          }
          <Toast
            backgroundColor='#ef8181'
            containerStyle={style.Toast}
            hideOnPress={false}
            position={100}
            shadowColor='white'
            textColor='black'
            textStyle={style.ToastText}
            visible={!this.state.connected}
          >Keine Internetverbindung!</Toast>
        </View>
      );
    } else if (!this.state.connected && this.state.fontLoaded)
      return (
        <View style={[style.MainView, style.StartMainView]}>
          <Toast
            backgroundColor='#ef8181'
            containerStyle={style.Toast}
            hideOnPress={false}
            position={100}
            shadowColor='white'
            textColor='black'
            textStyle={style.ToastText}
            visible={!this.state.connected}
          >Keine Internetverbindung!</Toast>
        </View>
      );
    else
      return (<View style={[style.MainView, style.StartMainView]} />);
  }
}
