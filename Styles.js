import { StyleSheet } from 'react-native';
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';

const style = StyleSheet.create({
  MainView: {
    flex: 1,
    backgroundColor: '#1C2431'
  },
  StartMainView: {
    alignItems: 'center',
    padding: scale(15),
    paddingTop: '20%'
  },
  GradeMainView: {
    paddingVertical: scale(15)
  },
  PlanMainView: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  CenterMainView: {
    justifyContent: 'center',
    flex: 1
  },
  SettingsMainView: {
    paddingVertical: scale(15)
  }
  ,
  IconButton: {
    flex: 1
  },
  IconButtonIcon: {
    marginHorizontal: scale(7)
  }
  ,
  LineView: {
    height: 1,
    backgroundColor: 'white',
    marginBottom: scale(15)
  }
  ,
  InfoText: {
    alignSelf: 'stretch',
    color: 'white',
    textAlign: 'center'
  },
  StartInfoText: {
    fontSize: moderateScale(20),
    marginBottom: verticalScale(15)
  },
  GradeInfoText: {
    fontSize: moderateScale(18),
    marginBottom: verticalScale(10),
    marginHorizontal: '4%',
  },
  PlanInfoText: {
    fontSize: moderateScale(24),
    marginHorizontal: scale(15)
  },
  DataInfoText: {
    fontSize: moderateScale(24),
    marginHorizontal: scale(15)
  },
  DataBottomInfoText: {
    fontSize: moderateScale(20),
    marginVertical: verticalScale(10)
  },
  SettingsInfoText: {
    fontSize: moderateScale(16),
    marginBottom: verticalScale(5),
    marginHorizontal: '4%'
  },
  ContactInfoText: {
    fontSize: moderateScale(24)
  }
  ,
  SubmitButton: {
    alignSelf: 'center',
    borderRadius: moderateScale(10),
    paddingVertical: verticalScale(5),
    shadowColor: '#000000',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowOpacity: 0.4,
    shadowRadius: 1,
    width: '50%'
  },
  StartSubmitButton: {
    marginTop: verticalScale(15)
  },
  GradeSubmitButton: {
    marginTop: scale(15)
  },
  SettingsSubmitButton: {
    marginTop: scale(10)
  }
  ,
  SubmitButtonOn: {
    backgroundColor: '#FFBC42'
  }
  ,
  SubmitButtonOff: {
    backgroundColor: 'grey'
  }
  ,
  SubmitButtonText: {
    backgroundColor: 'transparent',
    fontSize: moderateScale(24),
    textAlign: 'center'
  }
  ,
  TextField: {
    alignSelf: 'center',
    textAlign: 'center',
    width: '50%'
  },
  StartTextField: {
    color: '#FFBC42',
    fontSize: moderateScale(24)
  },
  SettingsTextField: {
    color: 'white',
    fontSize: moderateScale(48)
  }
  ,
  CourseLine: {
    marginHorizontal: '8%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  }
  ,
  PlanPlaceholder: {
    width: scale(50),
    height: scale(50)
  }
  ,
  Container: {
    backgroundColor: '#2E3649',
    borderRadius: moderateScale(15),
    padding: moderateScale(11),
    paddingTop: moderateScale(14),
    shadowColor: '#000000',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowOpacity: 0.4,
    shadowRadius: 1
  },
  PlanContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    height: scale(150),
    justifyContent: 'center',
    width: scale(150)
  },
  DataContainer: {
    marginBottom: verticalScale(13),
    marginHorizontal: scale(15)
  }
  ,
  ContainerText: {
    color: 'white'
  },
  PlanContainerText: {
    alignSelf: 'center',
    fontSize: scale(72)
  },
  DataContainerText: {
    alignSelf: 'stretch',
    fontSize: scale(20)
  }
  ,
  Badges: {
    flexDirection: 'row',
    marginLeft: scale(25),
    marginTop: verticalScale(3),
    top: verticalScale(10)
  }
  ,
  Badge: {
    borderRadius: moderateScale(15),
    height: verticalScale(20),
    justifyContent: 'center',
    paddingHorizontal: scale(7),
    shadowColor: '#000000',
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowOpacity: 0.4,
    shadowRadius: 1
  },
  PlanBadge: {
    alignSelf: 'flex-start',
    backgroundColor: '#FFBC42',
    marginLeft: scale(11),
    top: verticalScale(10)
  },
  DataBadge: {
    alignItems: 'center',
    marginRight: '2%'
  },
  InfoBadge: {
    backgroundColor: 'indianred'
  },
  LessonBadge: {
    backgroundColor: '#FFBC42'
  }
  ,
  BadgeText: {
    alignSelf: 'stretch',
    backgroundColor: 'transparent',
    color: 'black',
    fontSize: moderateScale(14)
  }
  ,
  Switch: {
    alignSelf: 'center',
    marginVertical: verticalScale(10)
  }
  ,
  SpinnerText: {
    color: '#FFBC42',
    textAlign: 'center'
  }
  ,
  Toast: {
    width: '75%'
  },
  ToastText: {
    alignSelf: 'stretch'
  }
});

export { style };
