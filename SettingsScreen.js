import React from 'react';
import { Alert, AppState, AsyncStorage, BackHandler, Keyboard, Linking, Platform, ScrollView, Switch, Text, TextInput, TouchableOpacity, View } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as Font from 'expo-font';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from './Toast.js';
import KeyboardSpacer from 'react-native-keyboard-spacer';
var jsdiff = require('diff');
import { style } from './Styles.js';

export default class DataScreen extends React.Component {
  static navigationOptions = {
    title: 'Einstellungen'
  };

  state = {
    appState: AppState.currentState,
    connected: true,
    dataLoaded: false,
    fontLoaded: false,
    isLoading: false,
    token: '',
    permissionGiven: false,
    notificationValue: false,
    daytime: '',
    lastUpdatedDaytime: ''
  };

  onBackButtonClicked = () => {
    this.props.navigation.goBack();
    return true;
  }

  getSettings = async () => {
    this.requestNotificationPermissions(false, false);
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS),
      permission = existingStatus === 'granted',
      token = permission ? await Notifications.getExpoPushTokenAsync() : '-';
    try {
      AsyncStorage.getItem('password')
        .then(async (password) => {
          if (password !== null)
            fetch('https://wetter.codes/gg/notification_datas', {
              method: 'POST',
              body: JSON.stringify({
                'password': password,
                'permission': permission,
                'token': token
              }),
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              }
            })
              .then(async (response) => response.json())
              .then(async (responseJson) => {
                if (responseJson.valid) {
                  const daytime = responseJson.daytime.slice(0, 2) + ':' + responseJson.daytime.slice(2);
                  this.setState({
                    isLoading: false,
                    dataLoaded: true,
                    notificationValue: responseJson.value,
                    daytime: daytime,
                    lastUpdatedDaytime: daytime
                  });
                } else {
                  Alert.alert(
                    'Fehler',
                    'Interner Serverfehler. Bitte versuche es zu einem späteren Zeitpunkt erneut.',
                    [
                      {
                        text: 'OK',
                        onPress: () => this.props.navigation.goBack()
                      }
                    ],
                    { cancelable: false }
                  );
                }
              })
              .catch(error => {
                console.error(error);
                this.setState({
                  notificationValue: false
                });
              }).done();
        }).done();
    } catch (error) {
      console.error(error);
    }
  }

  async requestNotificationPermissions(changeSwitchValue, giveFeedback) {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    var finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    if (finalStatus !== 'granted') {
      if (giveFeedback) {
        this.setState({ showSpinner: true });
        Alert.alert(
          'Fehler',
          'Du musst der App die Berechtigung geben, Dir Benachrichtigungen senden zu dürfen.',
          [
            {
              text: 'OK',
              onPress: () => {
                if (Platform.OS === 'ios') {
                  Linking.canOpenURL('app-settings:')
                    .then(supported => {
                      if (supported)
                        return Linking.openURL('app-settings:');
                    })
                    .catch(error => console.error(error))
                    .done();
                }
              }
            }
          ],
          { cancelable: true }
        );
      }
    } else {
      this.setState({
        permissionGiven: true,
        token: await Notifications.getExpoPushTokenAsync(),
        isLoading: changeSwitchValue ? true : this.state.isLoading
      });

      if (changeSwitchValue)
        this.changeNotification(true);
    }
  }

  handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active' && !this.state.permissionGiven)
      this.requestNotificationPermissions(false, true);
    this.setState({ appState: nextAppState });
  }

  handleConnectionChange = (isConnected) => {
    this.setState({ connected: isConnected });
    if (isConnected && this.state.daytime === '')
      this.getSettings();
  };

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonClicked);

    await Font.loadAsync({
      'sourcesanspro-semibold': require('./assets/fonts/SourceSansPro-SemiBold.ttf')
    });

    this.setState({ fontLoaded: true });

    this.unsubscribeNetInfo = NetInfo.addEventListener(state => this.handleConnectionChange(state.isConnected));

    NetInfo.fetch().then(state => {
      this.setState({ connected: state.isConnected });
      if (state.isConnected)
        this.getSettings();
    });

    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonClicked);

    this.unsubscribeNetInfo();

    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  changeNotification = async (value) => {
    try {
      AsyncStorage.getItem('password')
        .then(async (password) => {
          if (password !== null)
            AsyncStorage.getItem('classes')
              .then(async (classes) => {
                if (classes !== null)
                  fetch('https://wetter.codes/gg/notification', {
                    method: 'POST',
                    body: JSON.stringify({
                      'password': password,
                      'token': this.state.token,
                      'grade': classes,
                      'value': value
                    }),
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    }
                  })
                    .then(async (response) => response.json())
                    .then(async (responseJson) => {
                      if (responseJson.valid) {
                        this.setState({
                          notificationValue: value,
                          isLoading: false
                        })
                      } else {
                        this.setState({
                          isLoading: false
                        })
                        Alert.alert(
                          'Fehler',
                          'Interner Serverfehler. Bitte versuche es zu einem späteren Zeitpunkt erneut.',
                          [{ text: 'OK' }],
                          { cancelable: true }
                        );
                      }
                    })
                    .catch(error => {
                      console.error(error);
                      this.setState({
                        notificationValue: !value
                      });
                    }).done();
              }).done();
        }).done();
    } catch (error) {
      console.error(error);
    }
  }

  changeDaytime = () => {
    const { daytime, lastUpdatedDaytime } = this.state;
    try {
      AsyncStorage.getItem('password')
        .then(async (password) => {
          if (password !== null)
            fetch('https://wetter.codes/gg/daytime', {
              method: 'POST',
              body: JSON.stringify({
                'password': password,
                'token': this.state.token,
                'daytime': daytime.replace(':', '')
              }),
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              }
            })
              .then(async (response) => response.json())
              .then(async (responseJson) => {
                if (responseJson.valid) {
                  this.setState({
                    isLoading: false,
                    lastUpdatedDaytime: daytime
                  });
                  Alert.alert(
                    'Erfolgreich',
                    'Dir werden nun für jeden Schultag Benachrichtigungen um ' + daytime + ' Uhr gesendet.',
                    [{ text: 'OK' }],
                    { cancelable: true }
                  );
                } else {
                  this.setState({
                    isLoading: false,
                    daytime: lastUpdatedDaytime
                  });
                  Alert.alert(
                    'Fehler',
                    'Interner Serverfehler. Bitte versuche es zu einem späteren Zeitpunkt erneut.',
                    [{ text: 'OK' }],
                    { cancelable: true }
                  );
                }
              })
              .catch(error => console.error(error))
              .done();
        }).done();
    } catch (error) {
      console.error(error);
    }
  }

  daytimeMatches() {
    return this.state.daytime.match(new RegExp('^(([0-1][0-9])|(2[0-3])):[0-5][0-9]$'));
  }

  onDaytimeType = (newDaytime) => {
    if (Platform.OS === 'ios') {
      this.setState({ daytime: newDaytime });
      return;
    }

    const oldDaytime = this.state.daytime,
      datetimeParts = newDaytime.split(':');
    for (var i = 0; i < datetimeParts.length; i++)
      if (datetimeParts[i].length > 2) {
        this.setState({ daytime: oldDaytime });
        return;
      }

    const differences = jsdiff.diffChars(oldDaytime, newDaytime);
    for (var i = 0; i < differences.length; i++) {
      const difference = differences[i];
      if (difference.hasOwnProperty('added') && difference.hasOwnProperty('removed'))
        if (difference.added && !difference.value.match(new RegExp('^[0-9]$')) || difference.removed && difference.value.indexOf(':') !== -1) {
          this.setState({ daytime: oldDaytime });
          return;
        }
    }

    this.setState({ daytime: newDaytime });
  }

  render() {
    if (this.state.fontLoaded && this.state.dataLoaded) {
      return (
        <View style={[style.MainView, style.SettingsMainView]}>
          <ScrollView>
            <Text style={[style.InfoText, style.SettingsInfoText, {
              fontFamily: 'sourcesanspro-semibold'
            }]}>Möchtest Du für jeden Schultag zusätzlich eine Benachrichtigung erhalten, ob für Dich Vertretungsstunden relevant sind?</Text>
            <Switch
              disabled={this.state.isLoading}
              onTintColor='#92D050'
              onValueChange={(value) => {
                if (this.state.connected) {
                  if (value && !this.state.permissionGiven)
                    this.requestNotificationPermissions(true, true);
                  else {
                    this.setState({
                      isLoading: true
                    })
                    this.changeNotification(value);
                  }
                }
              }}
              style={style.Switch}
              thumbTintColor='#2E3649'
              tintColor='#CD5C5C'
              value={this.state.notificationValue}
            />
            {
              this.state.notificationValue ?
                <View>
                  <Text style={[style.InfoText, style.SettingsInfoText, {
                    fontFamily: 'sourcesanspro-semibold'
                  }]}>Gib hier die Zeit (im Format HH:MM) ein, zu der Dir die Benachrichtigung gesendet werden soll.{"\n"}Wenn die Zeit nach 16 Uhr ist, enthält die Benachrichtigung schon Infos für den nächsten Tag.</Text>
                  <TextInput
                    autoCorrect={false}
                    keyboardAppearance='dark'
                    keyboardType='numeric'
                    onChangeText={(text) => this.onDaytimeType(text)}
                    returnKeyType='done'
                    style={[style.TextField, style.SettingsTextField, {
                      fontFamily: 'sourcesanspro-semibold'
                    }]}
                    underlineColorAndroid='transparent'
                    value={this.state.daytime}
                  />
                  {
                    this.daytimeMatches() && this.state.daytime != this.state.lastUpdatedDaytime && !this.state.isLoading ?
                      <TouchableOpacity
                        style={[style.SubmitButton, style.SettingsSubmitButton, style.SubmitButtonOn]}
                        onPress={() => {
                          if (this.state.connected && this.state.permissionGiven) {
                            this.setState({
                              isLoading: true
                            });
                            Keyboard.dismiss();
                            this.changeDaytime();
                          }
                        }}
                      >
                        <Text style={[style.SubmitButtonText, {
                          fontFamily: 'sourcesanspro-semibold'
                        }]}>Auswählen</Text>
                      </TouchableOpacity> :
                      <View
                        style={[style.SubmitButton, style.SettingsSubmitButton, style.SubmitButtonOff]}
                      >
                        <Text style={[style.SubmitButtonText, {
                          fontFamily: 'sourcesanspro-semibold'
                        }]}>Auswählen</Text>
                      </View>
                  }
                  <KeyboardSpacer />
                </View> : null
            }
          </ScrollView>
          <View style={style.LineView} />
          <Text style={[style.InfoText, style.SettingsInfoText, style.ContactInfoText, {
            fontFamily: 'sourcesanspro-semibold'
          }]}>Kontakt:{'\n'}contact@wetter.codes</Text>
          <Toast
            backgroundColor='#ef8181'
            containerStyle={style.Toast}
            hideOnPress={false}
            position={100}
            shadowColor='white'
            textColor='black'
            textStyle={style.ToastText}
            visible={!this.state.connected}
          >Keine Internetverbindung!</Toast>
        </View>
      );
    } else if (!this.state.connected && this.state.fontLoaded)
      return (
        <View style={[style.MainView, style.SettingsMainView]}>
          <ScrollView />
          <View style={style.LineView} />
          <Text style={[style.InfoText, style.SettingsInfoText, style.ContactInfoText, {
            fontFamily: 'sourcesanspro-semibold'
          }]}>Kontakt:{'\n'}contact@wetter.codes</Text>
          <Toast
            backgroundColor='#ef8181'
            containerStyle={style.Toast}
            hideOnPress={false}
            position={100}
            shadowColor='white'
            textColor='black'
            textStyle={style.ToastText}
            visible={!this.state.connected}
          >Keine Internetverbindung!</Toast>
        </View>
      );
    else
      return (
        <View style={[style.MainView, style.SettingsMainView]}>
          <Spinner
            cancelable={false}
            color='#FFBC42'
            animation='slide'
            overlayColor='transparent'
            size='large'
            textContent='Lade Einstellungen..'
            textStyle={style.SpinnerText}
            visible={!this.state.dataLoaded && this.state.connected}
          />
        </View>
      );
  }
}
